import { not } from '@angular/compiler/src/output/output_ast';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'poster'
})
export class PosterPipe implements PipeTransform {

  transform(poster: string): string {
    // console.log(poster);
    // "https://image.tmdb.org/t/p/w500{{ movie.poster_path }}"
    if (poster === null || poster === undefined || poster === '' ||    poster.length === 0 || poster.trim() === 'null' ) {
      return './assets/no-image.jpg';
    } else {
      return `https://image.tmdb.org/t/p/w500/${ poster }`;
    }
    // return poster;
  }

}
