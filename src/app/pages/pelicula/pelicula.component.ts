import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PeliculasService } from '../../services/peliculas.service';
import { MovieResponse } from '../../interfaces/movie.response';
import { Location } from '@angular/common';
import { StarRatingComponent } from 'ng-starrating';
import { Cast } from '../../interfaces/credits-response';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-pelicula',
  templateUrl: './pelicula.component.html',
  styleUrls: ['./pelicula.component.css']
})
export class PeliculaComponent implements OnInit {

  movie: MovieResponse;
  cast: Cast[]=[];

  constructor( private activatedRoute: ActivatedRoute,
              private peliculasService: PeliculasService,
              private location: Location,
              private router: Router) { }

  ngOnInit(): void {
    // const id = this.activatedRoute.snapshot.params.id; // leyendo como variable
    const {id} = this.activatedRoute.snapshot.params; // metodo de desustructurar

    // console.log(id);

    combineLatest([
      this.peliculasService.getPeliculaDetalle(id),
      this.peliculasService.getCast(id)
    ]).subscribe( ([movie, cast]) => {
      // console.log(objeto);
      if (!movie) {
        this.router.navigateByUrl('/home');
        return;
      }
      this.movie = movie;

      this.cast = cast.filter( actor => actor.profile_path != null);
    });

    // this.peliculasService.getPeliculaDetalle(id).subscribe( movie => {
    //   if (!movie) {
    //     this.router.navigateByUrl('/home');
    //     return;
    //   }
    //   this.movie = movie;
    // });
    // // this.peliculasService.getCast(id).subscribe( cast => this.cast = cast); // importe todo
    // this.peliculasService.getCast(id).subscribe( cast => this.cast = cast.filter( actor => actor.profile_path != null) ); // filtra solo aquellos que tienen foto
  }

  onRegresar() {
    this.location.back();

  }

  onRate($event:{oldValue: number, newValue: number, starRating: StarRatingComponent}) {
    alert(`Old Value:${$event.oldValue},
      New Value: ${$event.newValue},
      Checked Color: ${$event.starRating.checkedcolor},
      Unchecked Color: ${$event.starRating.uncheckedcolor}`);
  }

}
